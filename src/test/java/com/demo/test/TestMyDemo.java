package com.demo.test;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.demo.MyDemo;

public class TestMyDemo {

	MyDemo m;
	@Before
	public void setUp() throws Exception {
		m=new MyDemo();
	}

	@After
	public void tearDown() throws Exception {
		m=null;
	}

	@Test
	public void testNumOfChar() {
	    assertEquals(5,m.numOfChar("hello"));
	}
	
	
	@Test
	public void testNumOfChar_false() {
		 assertEquals(4,m.numOfChar("hello"));
	}

}
